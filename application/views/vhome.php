<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="<?php echo HEADER_TITLE ?>">
        <meta property="title" content="<?php echo HEADER_TITLE ?>" />
		<meta property="og:title" content="<?php echo HEADER_TITLE ?>" />
		<meta property="og:image" content="<?php echo base_url('images/DSC_7262.jpg')?>?thn=1" />
		<meta property="og:url" content="<?php echo base_url()?>" />
		<meta property="og:description" content="We are Getting Married" />
		<meta property="twitter:title" content="Mr & Mrs Muslim Weddings" />
		<meta property="twitter:image:src" content="<?php echo base_url('images/DSC_7262.jpg')?>?thn=1" />
		
        <title><?php echo HEADER_TITLE ?></title>
		
		<!-- Mobile Specific Meta
		================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
		
		<!-- CSS
		================================================== -->
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/font-awesome.min.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/bootstrap.min.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/animate.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/owl.carousel.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/component.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/slit-slider.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/main.css')?>" />
		<link rel="stylesheet" href="<?php echo base_url('html/home/css/media-queries.css')?>" />


		<!--
		Google Font
		=========================== -->                    
		
		<!-- Oswald / Title Font -->
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
		<!-- Ubuntu / Body Font -->
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300' rel='stylesheet' type='text/css'>
		
		<!-- Modernizer Script for old Browsers -->
		<script src="<?php echo base_url('html/home/js/modernizr-2.6.2.min.js')?>"></script>

	
    </head>
	
    <body id="body">
	    <!--
	    Start Preloader
	    ==================================== -->
		<div id="loading-mask">
			<div class="loading-img">
				<img alt="Meghna Preloader" src="html/home/img/preloader.gif"  />
			</div>
		</div>
        <!--
        End Preloader
        ==================================== -->
		
        <!--
        Welcome Slider
        ==================================== -->
		<section id="home">	    
		
            <div id="slitSlider" class="sl-slider-wrapper">
				<div class="sl-slider">
					
					<!-- single slide item -->
					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
						<div class="sl-slide-inner">
							<div class="bg-img bg-img-1"></div>
						<div class="carousel-caption">
							<div>
								<h2 class="wow fadeInUp">MR & MRS <span class="color">MUSLIM</span></h2>
								<h2 data-wow-duration="500ms"  data-wow-delay="500ms" class="heading animated fadeInRight">WEDDINGS</h2>
							</div>
						</div>
						</div>
					</div>
					<!-- /single slide item -->
					
					<!-- single slide item -->
					<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
						<div class="sl-slide-inner">
							<div class="bg-img bg-img-2"></div>
						<div class="carousel-caption">
							<div>
								<h2 class="heading animated fadeInDown">FIRST COME LOVE</h2>
								<h3 class="animated fadeInUp">now come <span class="color">marriage</span></h3>
							</div>
						</div>
						</div>
					</div>
					<!-- /single slide item -->
					
					<!-- single slide item -->
					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
						<div class="sl-slide-inner">
							<div class="bg-img bg-img-3"></div>
						<div class="carousel-caption">
							<div>
								<h2 class="heading animated fadeInRight">IN THE END</h2>
								<h3 class="animated fadeInLeft">Your heart will find the right <span class="color">home</span></h3>

							</div>
						</div>
						</div>
					</div>
					<!-- /single slide item -->

				</div><!-- /sl-slider -->
				
				<nav id="nav-arrows" class="nav-arrows">
					<span class="nav-arrow-prev">Previous</span>
					<span class="nav-arrow-next">Next</span>
				</nav>

				<nav id="nav-dots" class="nav-dots">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
				</nav>

			</div><!-- /slider-wrapper -->
		</section>
		<!--/#home section-->
		
        <!-- 
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
                    <a class="navbar-brand" href="#body">
						<h1 id="logo">MR & MRS <span class="color">MUSLIM</span></h1>
					</a>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="Navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li class="current"><a href="#body">Home</a></li>
                        <li><a href="#couple">Couple</a></li>
                        <li><a href="#the-day">The Day</a></li>
                        <li><a href="#testimonial">Friends</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
				
            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->
		
		<!-- Start Couple Section -->
		<section id="couple">
			<div class="container">
				<div class="row">
				
					<!-- section title -->
					<div class="title text-center wow fadeInUp" data-wow-duration="500ms">
						<h2>Couple</h2>
						<div class="border"></div>
					</div>
					<!-- /section title -->
					
					<!-- Couple speech -->
					<div class="col-md-6 col-sm-12 wow fadeInDown" data-wow-duration="500ms">
                       <article class="couple-mate">
							<div class="member-photo">
								<img class="img-responsive" src="images/home/one.jpg" alt="Siska">
							</div>
							
							<!-- member name & designation -->
							<div class="member-title">
								<h3>Fransiska Fitriyanti</h3>
								<span>The Bride</span>
							</div>
							<!-- /member name & designation -->
							
							<!-- about member -->
                           <div class="member-info">
                               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur necessitatibus ullam, culpa odio.</p>
                           </div>
						   <!-- /about member -->
						   
                       </article>
                    </div>
					
					<div class="col-md-6 col-sm-12 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="200ms">
                       <article class="couple-mate">
							<div class="member-photo">
								<img class="img-responsive" src="images/home/two.jpg" alt="Wildan">
							</div>
							<div class="member-title">
								<h3>Wildan Muslim</h3>
								<span>The Groom</span>
							</div>
                           <div class="member-info">
                               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur necessitatibus ullam, culpa odio.</p>
                           </div>
						</article>
                    </div>
					<!-- end Couple speech -->
					
				</div>  	<!-- End row -->
			</div>   	<!-- End container -->
		</section>   <!-- End section -->

		<section id="parallax-1" class="parallax-section">
			<div class="container">
				<div class="row">
				</div>
			</div>   	<!-- end container -->
		</section>   <!-- end section -->
		
		<!-- the day -->		
		<section id="the-day">
			<div class="container">
				<div class="row">
					<!-- section title -->
					<div class="title text-center wow fadeIn" data-wow-duration="500ms">
						<h2>WHERE AND WHEN</h2>
						<h3>OF THE MARRIAGE</h3>
						<div class="border"></div>
					</div>
					<!-- /section title -->
					
					<article class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="500ms">
						<div class="service-block text-center">
							<div class="service-icon text-center">
								<i class="fa fa-map-marker fa-5x"></i>
							</div>
							<h3>Location</h3>
							<p>PUSSEN ARHANUD, Jalan Stasion, Jl. Sriwijaya Raya No. 1, Kel. Setiamanah, Kec. Cimahi Tengah, Setiamanah, Cimahi Tengah, Kota Cimahi</p>
						</div>
					</article>

					<article class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="200ms">
						<div class="service-block text-center">
							<div class="service-icon text-center">
								<i class="fa fa-calendar fa-5x"></i>
							</div>
							<h3>Date</h3>
							<p>Saturday October 07,2017 <br>Akad : 08.00 WIB, Resepsi : 11.00 - 14.00 WIB</p>
						</div>
					</article>
				
				</div> <!-- end row -->
			</div> <!-- end container -->
			
			<!-- Google Map -->
			<div class="google-map wow fadeInDown" data-wow-duration="500ms">
				<div id="map-canvas"></div>
			</div>	
			<!-- /Google Map -->
			
		</section> <!-- end section -->
		
		<section id="parallax-2" class="parallax-section">
			<div class="container">
				<div class="row">
				</div>
			</div>   	<!-- end container -->
		</section>   <!-- end section -->
		
		<!-- testimonial -->
		<section id="testimonial">
			<div class="container">
				<div class="row wow fadeInDown" data-wow-duration="500ms">
					<div class="col-lg-12">
					
						<!-- section title -->
						<div class="title text-center">
							<h2>Our <span class="color">Friends</span></h2>
							<div class="border"></div>
						</div>
						<!-- /section title -->
					
						<!-- portfolio item filtering -->
						<div class="portfolio-filter clearfix">
							<ul class="text-center">
							    <li><a href="javascript:void(0)" class="filter" data-filter="all">ALL</a></li>
								<li><a href="javascript:void(0)" class="filter" data-filter=".siska">BRIDESMAID</a></li>
								<li><a href="javascript:void(0)" class="filter" data-filter=".wildan">GROOMSMEN</a></li>
							</ul>
						</div>
						<!-- /portfolio item filtering -->
						
					</div> <!-- /end col-lg-12 -->
				</div> <!-- end row -->
			</div>	<!-- end container -->
	
			<!-- portfolio items -->
			<div class="portfolio-item-wrapper wow fadeInUp" data-wow-duration="500ms">
                <ul id="og-grid" class="og-grid">
				
					<!-- single portfolio item -->	
					<li class="mix wildan">
						<a href="javascript:void(0)" data-largesrc="images/home/friends/anggia-yuni.jpg" data-title="Anggia & Yuni" data-description="'Semoga Allah mengumpulkan kesempurnaan kalian berdua, membahagiakan kesungguhan kalian berdua, memberkahi kalian berdua, dan mengeluarkan kebajikan dari kalian berdua yang banyak.' (Doa Rasulullah SAW saat menikahkan Fatimah Az-Zahra') H.R Anas Ibn Malik r.a <br>
						Barakallahu laka wa baraka 'alaika wa jama'a baynakuma fii khair. Wildan dan Siska<br>
						Menjadi keluarga yang sakinah, mawadah dan warahmah sampai ke surganya. Aamiin">
							<img src="images/home/friends/anggia-yuni.jpg" alt="Anggia & Yuni">
							<div class="hover-mask">
								<h3>Anggia & Yuni</h3>
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
							</div>
						</a>
					</li>
					<!-- /single portfolio item -->
					
					<!-- single portfolio item -->
					<li class="mix siska">
						<a href="javascript:void(0)" data-largesrc="images/home/friends/portx1.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
							<img src="images/home/friends/portx1.jpg" alt="Meghna">
							<div class="hover-mask">
								<h3>Veggies sunt bona vobis</h3>
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
							</div>
						</a>
					</li>
					<!-- /single portfolio item -->
					
					<!-- single portfolio item -->
					<li class="mix wildan">
						<a href="javascript:void(0)" data-largesrc="images/home/friends/portx1.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
							<img src="images/home/friends/portx1.jpg" alt="Meghna">
							<div class="hover-mask">
								<h3>Dandelion horseradish</h3>
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
							</div>
						</a>
					</li>
					<!-- /single portfolio item -->
					
					<!-- single portfolio item -->
					<li class="mix wildan">
						<a href="javascript:void(0)" data-largesrc="images/home/friends/portx1.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
							<img src="images/home/friends/portx1.jpg" alt="Meghna">
							<div class="hover-mask">
								<h3>Dandelion horseradish</h3>
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
							</div>
						</a>
					</li>
					<!-- /single portfolio item -->
					
					<!-- single portfolio item -->
					<li class="mix siska">
						<a href="javascript:void(0)" data-largesrc="images/home/friends/portx1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
							<img src="images/home/friends/portx1.jpg" alt="Meghna">
							<div class="hover-mask">
								<h3>Azuki bean</h3>
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
							</div>
						</a>
					</li>
					<!-- /single portfolio item -->
					
					<!-- single portfolio item -->
					<li class="mix siska">
						<a href="javascript:void(0)" data-largesrc="images/home/friends/portx1.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
							<img src="images/home/friends/portx1.jpg" alt="Meghna">
							<div class="hover-mask">
								<h3>Veggies sunt bona vobis</h3>
								<span>
									<i class="fa fa-plus fa-2x"></i>
								</span>
							</div>	
						</a>
					</li>
					<!-- /single portfolio item -->
					
					
					
				</ul> <!-- end og grid -->
			</div>  <!-- portfolio items wrapper -->
			
		</section>   <!-- End section -->

		<footer id="footer" class="bg-one">
			<div class="container">
			    <div class="row" data-wow-duration="500ms">
					<div class="col-lg-12">
						<!-- copyright -->
						<div class="copyright text-center">							
							<p>mrandmrsmuslim.com. Copyright &copy; 2017. All Rights Reserved.</p>
						</div>
						<!-- /copyright -->
						
					</div> <!-- end col lg 12 -->
				</div> <!-- end row -->
			</div> <!-- end container -->
		</footer> <!-- end footer -->
		
		<!-- Back to Top
		============================== -->
		<a href="javascript:;" id="scrollUp">
			<i class="fa fa-angle-up fa-2x"></i>
		</a>
		
		<!-- end Footer Area
		========================================== -->
		
		<!-- 
		Essential Scripts
		=====================================-->
		

		<script src="<?php echo base_url('html/home/js/jquery-1.11.0.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/bootstrap.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.slitslider.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.ba-cond.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.parallax-1.1.3.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/owl.carousel.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.mixitup.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.nicescroll.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.appear.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/easyPieChart.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.easing-1.3.pack.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/tweetie.min.js')?>"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyC3iodfURpRE81Y5Av6kOAj3fqeXOvgDgU&sensor=false"></script>
		<script src="<?php echo base_url('html/home/js/jquery.nav.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.sticky.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.countTo.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/wow.min.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/jquery.fitvids.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/grid.js')?>"></script>
		<script src="<?php echo base_url('html/home/js/custom.js')?>"></script>


    </body>
</html>